# Wisdom Search - take home task

## Installation
Set the correct node version in the project root dir.
```
nvm use
```

### Backend
```
cd <app_directory>/backend
cp .env.dist .env
```

Fill the env values, respectively.

```
npm i
npm run test -- runs all test
npm run dev
```
Backend should be up and running.

### Frontend
```
cd <app_directory>/frontend
npm i
npm run start
```

I didn't configure envvar for server url, it's in the code :(.

