import './App.css';
import { useEffect, useState } from "react";
import axios from "axios";
import Results from "./components/Results";

function App() {
  const [query, setQuery] = useState('')
  const [results, setResults] = useState([])
  const [nextLink, setNextLink] = useState(null)
  const [previousLink, setPreviousLink] = useState(null)

  const submit = (query, page) => {
    let url = `http://localhost:4000/search?q=${query}`
    if (page) {
      url += `&page=${page}`
    }

    axios.get(url).then(response => {
      setPreviousLink(response.data.previousLink)
      setNextLink(response.data.nextLink)
      setResults(response.data.data)
    })
  }

  useEffect(() => {
    const searchParams = new URLSearchParams(window.location.search)
    const queryFromURL = searchParams.get('q');
    const page = searchParams.get('page') || 1

    if (queryFromURL) {
      submit(queryFromURL, page)
      setQuery(queryFromURL)
    }
  }, [])

  return (
    <div className="App">
      <main className="App-content">
        <p>
          Wisdom Search
        </p>
        <div>
          <input name="queryInput" value={query} onChange={value => setQuery(value.target.value)} />
          <button name="submitButton" onClick={() => window.location.href = `/?q=${query}&page=1`}>Search</button>
        </div>
        <div className="App-navigation">
          {(previousLink) &&
              <button
                  style={{ visibility: previousLink ? 'visible' : 'hidden' }}
                  onClick={() => window.location.href = previousLink}>
                Previous
              </button>
          }
          {(nextLink) &&
              <button
                  style={{ visibility: nextLink ? 'visible' : 'hidden' }}
                  onClick={() => window.location.href = nextLink}>
                Next
              </button>
          }
        </div>
        <Results results={results}/>
      </main>
    </div>
  );
}

export default App;
