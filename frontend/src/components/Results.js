import './Results.css';
import axios from "axios";

function Results(props) {
    const { results } = props

    function registerClick(id) {
        axios.post('http://localhost:4000/click?id=' + id)
    }

    return (
        <ul>
            {results.map(
                (result, index) => (
                    <li key={index}>
                        <a onClick={() => registerClick(result.id)}>
                            <span>{result.id}</span>
                            <pre className="data-demo">
                                {result.data}
                            </pre>
                        </a>
                    </li>)
            )}
        </ul>
    )
}

export default Results;