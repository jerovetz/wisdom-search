import { Document } from "./localCorpus";
import axios from "axios";

export default class OMDBApi {
    private readonly key: string

    static BASE_URL = 'http://www.omdbapi.com/'

    constructor(apiKey: string) {
        this.key = apiKey
    }

    public async fetchTitlesBy(keyword : string) : Promise<Document[]> {
        const { data: response } = await axios.get(OMDBApi.BASE_URL + `?apikey=${this.key}&s=${keyword}`)
        if (response.Response !== 'True') {
            return []
        }
        return response.Search.map(item => ({
            id: item.Title,
            data: JSON.stringify(item),
            clickCount: 0
        }))
    }

}