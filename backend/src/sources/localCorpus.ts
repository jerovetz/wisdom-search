import * as fs from "fs";

export type Result = {
    id: string,
    clickCount: number
}

export type Document = {
    id: string,
    data: string,
    clickCount: number
}

export class LocalCorpus {

    private corpus: Document[]
    private readonly dataPath: string

    constructor(path : string) {
        this.dataPath = path
        this.corpus = []
    }

    public initialize() : LocalCorpus {
        const files = fs.readdirSync(this.dataPath)
        files.forEach(fileName => {
            this.corpus.push({
                data: fs.readFileSync(`${this.dataPath}/${fileName}`).toString(),
                id: fileName,
                clickCount: 0
            })
        })
        return this
    }

    public search(keyword: string): Document[] {
        return this.corpus
            .filter(document => document.data.indexOf(keyword) !== -1)
    }

    public click(id: string) {
        this.corpus.find(document => document.id == id).clickCount++
    }

    public exists(id: string): boolean {
        return this.corpus.find(document => document.id == id) !== undefined
    }

    public set(id: string) {
        this.corpus.push({
            id,
            data: id,
            clickCount: 0
        })
    }


}