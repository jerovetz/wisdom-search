import Koa from 'koa';
import Router from 'koa-router'
import cors from '@koa/cors'
import 'dotenv/config'
import { LocalCorpus } from "./sources/localCorpus"
import ResultGenerator from "./services/resultGenerator";
import OMDBApi from "./sources/omdbApi";

const app = new Koa()
const router = new Router()
const localCorpus = new LocalCorpus(process.env.DATA_PATH).initialize()

router.get('/search', async ctx => {
    const searchQuery = ctx.request.query.q.toString()
    const page = parseInt(ctx.request.query.page.toString()) || 1
    const generator = new ResultGenerator(localCorpus, new OMDBApi(process.env.OMDB_API_KEY), 10)
    ctx.body = await generator.generate(searchQuery, page)
    ctx.status = 200
})

router.post('/click', ctx => {
    const id = ctx.request.query.id.toString()
    if (localCorpus.exists(id) == false) {
        localCorpus.set(id)
    }
    localCorpus.click(id)
    ctx.status = 200
})

app.use(cors({ origin: '*' }))
app.use(router.routes())

app.listen(4000)
