import OMDBApi from "../sources/omdbApi";
import { LocalCorpus, Document } from "../sources/localCorpus";

export default class ResultGenerator {
    private corpus: LocalCorpus;
    private omdb: OMDBApi;
    private readonly pageSize: number;

    constructor(files: LocalCorpus, omdb: OMDBApi, pageSize: number) {
        this.corpus = files
        this.omdb = omdb
        this.pageSize = pageSize
    }

    public async generate(keyword: string, page: number) {
        const paginate = (results : Document[]) => {
            return results.slice((page - 1) * this.pageSize, page * this.pageSize)
        }

        const omdbResults = await this.omdb.fetchTitlesBy(keyword)
        const allResults = this.corpus.search(keyword)
        const ids = allResults.map(result => result.id)
        omdbResults.forEach(omdbResult => {
            if (ids.indexOf(omdbResult.id) == -1) {
                allResults.push(omdbResult)
            }
        })
        const resultsData = paginate(allResults.sort(
            (result1, result2) => result1.clickCount < result2.clickCount ? 1 : -1
        ))

        return {
            data: resultsData,
            previousLink: page > 1 ?`${process.env.APP_URL}/search?q=${keyword}&page=${page - 1}` : null,
            nextLink: allResults[page * this.pageSize] != undefined ? `${process.env.APP_URL}/search?q=${keyword}&page=${page + 1}` : null
        }
    }

}