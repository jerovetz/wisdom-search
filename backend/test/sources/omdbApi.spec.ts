import OMDBApi from "../../src/sources/omdbApi";
import { Document } from "../../src/sources/localCorpus";
import axios from "axios";

describe('OMDB Api', () => {

    test('fetches titles contains the keyword', async () => {
        const expected : Document[] = [
            {
               id: 'Mission: Impossible - Ghost Protocol',
               data: expect.anything(),
               clickCount: 0,
            },
            {
                id: 'Mission: Impossible',
                clickCount: 0,
                data: expect.anything()
            }
        ]

        const axiosResponse = {
            data:  {
                "Response": "True",
                "Search": [
            {
                "Title": "Mission: Impossible - Ghost Protocol",
                "Year": "2011",
                "imdbID": "tt1229238",
                "Type": "movie",
                "Poster": "https://m.media-amazon.com/images/M/MV5BMTY4MTUxMjQ5OV5BMl5BanBnXkFtZTcwNTUyMzg5Ng@@._V1_SX300.jpg"
            },
            {
                "Title": "Mission: Impossible",
                "Year": "1996",
                "imdbID": "tt0117060",
                "Type": "movie",
                "Poster": "https://m.media-amazon.com/images/M/MV5BOTFhMWY3ZTctNTJlOC00Y2UwLThmOGUtMWU4NDI1Yzg4ODRkXkEyXkFqcGdeQXVyMTUzMDUzNTI3._V1_SX300.jpg"
            }]
        }}
        const apiKey = 'apikey';
        const expectedUrl = `http://www.omdbapi.com/?apikey=${apiKey}&s=keyword3434`

        const get = jest.spyOn(axios, 'get').mockResolvedValue(axiosResponse)

        const results = await new OMDBApi(apiKey).fetchTitlesBy('keyword3434')

        expect(get).toHaveBeenCalledWith(expectedUrl)
        expect(results).toEqual(expected)
    })

    test('returns an empty array, if no movie found', async () => {
        const axiosResponse = {
            data:  { Response: 'False', Error: 'Movie not found!' }
        }
        const apiKey = 'apikey';
        jest.spyOn(axios, 'get').mockResolvedValue(axiosResponse)

        const results = await new OMDBApi(apiKey).fetchTitlesBy('keyword3434')
        expect(results).toEqual([])
    })

})