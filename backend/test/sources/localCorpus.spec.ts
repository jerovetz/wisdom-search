import { LocalCorpus, Document } from "../../src/sources/localCorpus";

describe('Local Corpus', () => {

    test('search after initializing returns results', () => {
        const expectedResults: Document[] = [
            {
                clickCount: 0,
                id: 'road_not_taken.txt',
                data: expect.anything()
            },
            {
                clickCount: 0,
                id: 'woods.txt',
                data: expect.anything()
            }
        ]

        const corpus = new LocalCorpus('./datasets/basic_text').initialize()
        const results = corpus.search('wood')

        expect(results).toEqual(expectedResults)
    })

    test('search after initializing returns docs which contain keyword', () => {
        const expectedResults: Document[] = [
            {
                clickCount: 0,
                id: 'road_not_taken.txt',
                data: expect.anything()
            }
        ]

        const corpus = new LocalCorpus('./datasets/basic_text').initialize()
        const results = corpus.search('Two roads')

        expect(results).toEqual(expectedResults)
    })

    test('incrementClick increments click of the specified document', () => {
        const corpus = new LocalCorpus('./datasets/basic_text').initialize()
        corpus.click('road_not_taken.txt')

        const results = corpus.search('Two roads')
        expect(results[0].clickCount).toEqual(1)

        const results2 = corpus.search('Whose woods')
        expect(results2[0].clickCount).toEqual(0)
    })

})