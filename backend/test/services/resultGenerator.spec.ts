import ResultGenerator from "../../src/services/resultGenerator";
import OMDBApi from "../../src/sources/omdbApi";
import { LocalCorpus, Document } from "../../src/sources/localCorpus";

describe('Result Generator', () => {

    test('generate merges both sources ordered by click count, only not stored ones added from OMDB', async () => {
        const omdbResults : Document[] = [
            {
                id: 'new one',
                clickCount: 0,
                data: 'raw json1'
            },
            {
                id: 'already stored',
                clickCount: 0,
                data: 'raw json2'
            }
        ]

        const fileResults: Document[] = [
            {
                id: 'file1',
                clickCount: 2,
                data: 'data1'
            },
            {
                id: 'already stored',
                clickCount: 12,
                data: 'data2'
            },
            {
                id: 'file0',
                clickCount: 1,
                data: 'data3'
            }
        ]

        const expected = {
            nextLink: null,
            previousLink: null,
            data: [ fileResults[1], fileResults[0], fileResults[2], omdbResults[0]]
        }

        const omdbApi = new OMDBApi('key')
        const localCorpus = new LocalCorpus('path')

        const omdb = jest.spyOn(omdbApi, 'fetchTitlesBy').mockResolvedValue(omdbResults)
        const fileSearch = jest.spyOn(localCorpus, 'search').mockReturnValue(fileResults)

        const keyword = 'keyword'
        const mergedResult = await new ResultGenerator(localCorpus, omdbApi, 10).generate(keyword, 1)

        expect(mergedResult).toEqual(expected)
        expect(omdb).toHaveBeenCalledWith(keyword)
        expect(fileSearch).toHaveBeenCalledWith(keyword)

    })

    test('gets later page', async () => {
        const omdbResults : Document[] = [
            {
                id: 'new one',
                clickCount: 0,
                data: 'json raw1'
            },
            {
                id: 'already stored',
                clickCount: 0,
                data: 'json raw2'
            }
        ]

        const fileResults: Document[] = [
            {
                id: 'file1',
                clickCount: 2,
                data: 'data1'
            },
            {
                id: 'already stored',
                clickCount: 12,
                data: 'data2'
            },
            {
                id: 'file0',
                clickCount: 1,
                data: 'data3'
            }
        ]
        const keyword = 'keyword'

        const expected = {
            nextLink: null,
            previousLink: `${process.env.APP_URL}/search?q=${keyword}&page=1`,
            data: [fileResults[2], omdbResults[0]]
        }

        const omdbApi = new OMDBApi('key')
        const localCorpus = new LocalCorpus('path')

        const omdb = jest.spyOn(omdbApi, 'fetchTitlesBy').mockResolvedValue(omdbResults)
        const fileSearch = jest.spyOn(localCorpus, 'search').mockReturnValue(fileResults)

        const mergedResult = await new ResultGenerator(localCorpus, omdbApi, 2).generate(keyword, 2)

        expect(mergedResult).toEqual(expected)
        expect(omdb).toHaveBeenCalledWith(keyword)
        expect(fileSearch).toHaveBeenCalledWith(keyword)

    })

    test('gets first page', async () => {
        const omdbResults : Document[] = [
            {
                id: 'new one',
                clickCount: 0,
                data: 'json raw1'
            },
            {
                id: 'already stored',
                clickCount: 0,
                data: 'json raw2'
            }
        ]

        const fileResults: Document[] = [
            {
                id: 'file1',
                clickCount: 2,
                data: 'data1'
            },
            {
                id: 'already stored',
                clickCount: 12,
                data: 'data2'
            },
            {
                id: 'file0',
                clickCount: 1,
                data: 'data3'
            }
        ]
        const keyword = 'keyword'

        const expected = {
            previousLink: null,
            nextLink: `${process.env.APP_URL}/search?q=${keyword}&page=2`,
            data: [fileResults[1], fileResults[0]]
        }

        const omdbApi = new OMDBApi('key')
        const localCorpus = new LocalCorpus('path')

        const omdb = jest.spyOn(omdbApi, 'fetchTitlesBy').mockResolvedValue(omdbResults)
        const fileSearch = jest.spyOn(localCorpus, 'search').mockReturnValue(fileResults)

        const mergedResult = await new ResultGenerator(localCorpus, omdbApi, 2).generate(keyword, 1)

        expect(mergedResult).toEqual(expected)
        expect(omdb).toHaveBeenCalledWith(keyword)
        expect(fileSearch).toHaveBeenCalledWith(keyword)

    })

    test('gets last page with orphan count', async () => {
        const omdbResults : Document[] = [
            {
                id: 'new one',
                clickCount: 0,
                data: 'json raw1'
            },
            {
                id: 'already stored',
                clickCount: 0,
                data: 'json raw2'
            }
        ]

        const fileResults: Document[] = [
            {
                id: 'file1',
                clickCount: 2,
                data: 'data1'
            },
            {
                id: 'already stored',
                clickCount: 12,
                data: 'data2'
            },
            {
                id: 'file0',
                clickCount: 1,
                data: 'data3'
            }
        ]
        const keyword = 'keyword'

        const expected = {
            previousLink: `${process.env.APP_URL}/search?q=${keyword}&page=1`,
            nextLink: null,
            data: [omdbResults[0]]
        }

        const omdbApi = new OMDBApi('key')
        const localCorpus = new LocalCorpus('path')

        const omdb = jest.spyOn(omdbApi, 'fetchTitlesBy').mockResolvedValue(omdbResults)
        const fileSearch = jest.spyOn(localCorpus, 'search').mockReturnValue(fileResults)

        const mergedResult = await new ResultGenerator(localCorpus, omdbApi, 3).generate(keyword, 2)

        expect(mergedResult).toEqual(expected)
        expect(omdb).toHaveBeenCalledWith(keyword)
        expect(fileSearch).toHaveBeenCalledWith(keyword)
    })
})